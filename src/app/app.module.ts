import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppCoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { NotesModule } from './modules/notes/notes.module';
import { SearchModule } from './modules/search/search.module';
import { UserModule } from './modules/users/user.module';
import { AppNotesModule } from './modules/appnotes/appnotes.module';
import { RouterModule,Routes } from '@angular/router';
import {BrowserModule} from '@angular/platform-browser';

const appRoutes: Routes = [
  {
      path: 'notes',
      loadChildren: () => AppNotesModule
  },
  {
      path: 'user',
      loadChildren: () => UserModule
  },
  {
      path:'',
      pathMatch:'full',
      redirectTo:'/user'
  }
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppCoreModule,
    SharedModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
