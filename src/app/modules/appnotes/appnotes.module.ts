import { NgModule } from "@angular/core";
import { AppNotesComponent } from "./appnotes.component";
import { SharedModule } from "../../shared/shared.module";
import { NotesModule } from "../notes/notes.module";
import { SearchModule } from "../search/search.module";
import { AppCoreModule } from "../../core/core.module";
import { RouterModule,Routes } from "@angular/router";


const noteRoutes: Routes = [
    {
        path: '',
        component: AppNotesComponent
    }
];

@NgModule({
    declarations:[AppNotesComponent],
    imports:[NotesModule,SearchModule,RouterModule.forChild(noteRoutes)],
    exports:[AppNotesComponent,NotesModule,SearchModule],

})
export class AppNotesModule {}