import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NotesService } from '../../shared/notes.service';
import * as model from '../../shared/models';
import { trigger, state, animate, transition, style, keyframes } from '@angular/animations';


@Component({
    selector: 'app-notes-component',
    templateUrl: 'notes.component.html',
    styleUrls: ['./notes.component.css'],
    animations: [
        trigger('noteState', [
            state('in', style({ transform: 'translateX(0)' })),
            transition('void => *', [
                // style({ transform: 'translateX(-100%)' }),
                // animate('100ms ease-in')
                animate(300, keyframes([
                    style({opacity: 0, transform: 'translateX(-100%)', offset: 0}),
                    style({opacity: 1, transform: 'translateX(15%)', offset: 0.4}),
                    style({opacity: 1, transform: 'translateX(0)', offset: 1}),
                ])
                )
            ]),
            transition('* => void', [
                animate(300, keyframes([
                    style({opacity: 1, transform: 'translateX(0)', offset: 0}),
                    style({opacity: 1, transform: 'translateX(-15%)', offset: 0.6}),
                    style({opacity: 0, transform: 'translateX(100%)', offset: 1}),
                ])
                )
            ])
        ])]
})

export class NotesComponent implements OnInit {
    constructor(public noteService: NotesService) { }
    //public notes: Array<model.NoteModel>;
    public activeDate : Date = new Date();
    public expiringDate : Date = new Date();
    public expiredDate : Date = new Date();
    get notes() : Array<model.NoteModel>{
        return this.noteService.notesData;
    }
    ngOnInit() {
        this.activeDate.setDate(this.expiredDate.getDate() + 8);
        this.expiringDate.setDate(this.expiredDate.getDate() + 7);
        //this.notes = this.noteService.notesData;
        // this.noteService.getData().subscribe(
        //     notes =>
        //     this.notes = notes
        // );
    }
}

