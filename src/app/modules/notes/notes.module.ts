import { NgModule } from '@angular/core';

import { NotesComponent } from './notes.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppCoreModule } from '../../core/core.module';
import { NoteComponent } from './note.component';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
    imports: [BrowserAnimationsModule,AppCoreModule,SharedModule],
    exports: [NotesComponent],
    declarations: [
    NotesComponent,
    NoteComponent],
    providers: [],
})
export class NotesModule { }


