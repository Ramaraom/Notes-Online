import { Component, OnInit, Input } from '@angular/core';
import { NoteModel } from '../../shared/models';
import { MatDialog } from '@angular/material';
import { CreateDialogComponent } from '../search/create-dialog.component';
import { NotesService } from '../../shared/notes.service';
import { trigger, state, animate, transition, style, keyframes } from '@angular/animations';


@Component({
    selector: 'app-note-component',
    templateUrl: 'note.component.html',
    styleUrls: ['./note.component.css'],
    animations: [
        trigger('noteUpdate', [
            transition('initial => update', [
                animate(200, keyframes([
                    style({transform: 'scale(0.9)', offset: 0}),
                    style({transform: 'scale(1.1)', offset: 0.5}),
                    style({transform: 'scale(1)', offset: 1}),
                ]))
            ])
        ])
    ]
})
export class NoteComponent implements OnInit {
    public updateState = 'initial';
    constructor(public dialog: MatDialog, public noteService: NotesService) { }
    // tslint:disable-next-line:no-input-rename
    @Input('note-data') noteData: NoteModel;
    ngOnInit() { }
    openDialog(): void {
        this.updateState = 'initial';
        const dialogRef = this.dialog.open(CreateDialogComponent, {
            width: '400px',
            data: this.noteData
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log(result);
            this.updateState = 'update';
            if (result.content) {
                this.noteData.content = result.content;
                this.noteData.dateTime = result.dateTime;
                this.noteData.type = result.type;
                this.noteData.image = "assets/images/placeholder.png";
                this.noteService.updateData(this.noteData);
            }
        });
    }

    deleteNote(): void {
        this.noteService.deleteNote(this.noteData);
    }
}
