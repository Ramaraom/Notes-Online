import {NgModule} from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AppCoreModule } from '../../core/core.module';
import {UserComponent} from './user.component';
import { RouterModule,Routes } from '@angular/router';

const userRoutes: Routes = [
    {
        path: '',
        component: UserComponent
    }
];

@NgModule({
    declarations:[UserComponent],
    imports:[AppCoreModule, SharedModule,RouterModule.forChild(userRoutes)],
    exports:[UserComponent],
    providers:[]
})
export class UserModule{ }