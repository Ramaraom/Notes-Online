import { Component, OnInit } from '@angular/core';
import { FormBuilder,Validators,FormGroup } from '@angular/forms';

@Component({
    selector: 'user-component',
    templateUrl: './user.component.html',
    styles: ['./user.component.css']
})
export class UserComponent implements OnInit {
    public loginForm : FormGroup;
    constructor(public fb: FormBuilder) {}
    ngOnInit() {
        this.loginForm = this.fb.group({
            userName : ['',Validators.required],
            password:['',Validators.required]
        });
    };

    onLogin = (values) => {
        let userName = values.userName;
        let password = values.password;
    };

}