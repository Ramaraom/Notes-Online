import { Component, OnInit } from '@angular/core';
import { MatDialog } from '../../../../node_modules/@angular/material';
import { CreateDialogComponent } from './create-dialog.component';
import { NotesService } from '../../shared/notes.service';

@Component({
    selector: 'app-create-component',
    templateUrl: 'create.component.html'
})

export class CreateComponent implements OnInit {
    constructor(public dialog: MatDialog, public noteService: NotesService) { }

    ngOnInit() { }

    openDialog(): void {
        const dialogRef = this.dialog.open(CreateDialogComponent, {
            width: '400px',
            data: {}
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log(result);
            if (result.content) {
            this.noteService.addData(result);
            }
        });
    }
}
