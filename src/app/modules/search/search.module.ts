import { NgModule } from '@angular/core';

import { SearchComponent } from './search.component';
import { CreateComponent } from './create.component';
import { AppCoreModule } from '../../core/core.module';
import { CreateDialogComponent } from './create-dialog.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
    imports: [AppCoreModule, SharedModule],
    exports: [CreateComponent],
    declarations: [
    SearchComponent,
    CreateComponent,
    CreateDialogComponent],
    providers: [],
    entryComponents: [CreateComponent, CreateDialogComponent]
})
export class SearchModule { }
