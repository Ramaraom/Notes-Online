import { Component, OnInit } from '@angular/core';
import { NotesService } from '../../shared/notes.service';


@Component({
    selector: 'app-search-component',
    templateUrl: 'search.component.html'
})

export class SearchComponent implements OnInit {
    public searchText : string;
    constructor(public notesService: NotesService) { }

    ngOnInit() { }

    onSearch() : void {
        this.notesService.filterSearch(this.searchText);
    }
}
