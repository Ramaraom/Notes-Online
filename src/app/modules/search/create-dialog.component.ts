import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '../../../../node_modules/@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import * as model from '../../shared/models';

export interface NotesType {
    id: number;
    value: string;
}

@Component({
    selector: 'app-create-dialog',
    templateUrl: 'create-dialog.component.html',
    styleUrls: ['./create-dialog.component.css']
})
export class CreateDialogComponent implements OnInit {
    public typeList: NotesType[];
    public isNewNote: boolean;
    public createForm = this.fb.group({
        type: [this.data.type, Validators.required],
        dateTime: [this.data.dateTime],
        content: [this.data.content, Validators.required],
        image: [this.data.image]
    });
    constructor(public dialogRef: MatDialogRef<CreateDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: model.NoteModel, public fb: FormBuilder) {
     }

    ngOnInit() {
        if (this.data.id != null && this.data.id !== undefined) {
            this.isNewNote = false;
        } else {
            this.isNewNote = true;
        }
        this.typeList = [{id: 1 , value: 'Text' }, {id: 2 , value: 'Image' }];
    }

    onCloseClick() {
        this.dialogRef.close();
    }

    onCreateClick(data) {
        data.image = "assets/images/placeholder.png";
        this.dialogRef.close(data);
    }
    onDateInput(event) {
    }
}


