import {FlexLayoutModule} from '@angular/flex-layout';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgModule, Optional, SkipSelf} from '@angular/core';
import {MatButtonModule,
  MatIconModule,
  MatListModule,
  MatSidenavModule,
  MatToolbarModule,
  MatInputModule,
  MatFormFieldModule,
  MatDialogModule,
  MatSelectModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatCardModule} from '@angular/material';
import { throwIfAlreadyLoaded } from './ensureCodeModuleLoadedOnlyOnce';
import { HeaderComponent } from './header.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

  @NgModule({
      declarations: [HeaderComponent],
      imports: [
          FlexLayoutModule,
          
          CommonModule,
          FormsModule,
          MatButtonModule,
          MatIconModule,
          MatListModule,
          MatSidenavModule,
          MatToolbarModule,
          MatInputModule,
          MatFormFieldModule,
          MatDatepickerModule,
          MatSelectModule,
          MatDialogModule,
          MatNativeDateModule,
          ReactiveFormsModule,
          HttpClientModule,
          MatCardModule,
          RouterModule
          ],
      exports: [
        MatIconModule,
        FlexLayoutModule,
          
          CommonModule,
          FormsModule,
          MatButtonModule,
          MatIconModule,
          MatListModule,
          MatSidenavModule,
          MatToolbarModule,
          MatInputModule,
          MatSelectModule,
          MatDatepickerModule,
          MatFormFieldModule,
          MatDialogModule,
          MatNativeDateModule,
          HeaderComponent,
          ReactiveFormsModule,
          HttpClientModule,
          MatCardModule,
          
      ],
      providers: []
  })
  export class AppCoreModule { 
    constructor(@Optional() @SkipSelf() parentModule: AppCoreModule) {
        throwIfAlreadyLoaded(parentModule, 'CoreModule');
    }
  }
