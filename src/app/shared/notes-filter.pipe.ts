import { Pipe, PipeTransform } from '@angular/core'
import * as model from './models'

@Pipe({
    name: 'notesFilter',
    pure: false
})
export class NotesFilter implements PipeTransform {
    transform(value: Array<model.NoteModel>, operator: string, date?: Date, date2?: Date): Array<model.NoteModel> {
        if (value) {
            return value.filter((note) => {
                var noteDate = note.dateTime;
                if (noteDate == null || noteDate === undefined) {
                    noteDate = new Date();
                    noteDate.setDate(noteDate.getDate() + 30);
                }
                if (operator === 'less') {
                    return noteDate < date;
                } else if (operator === 'between') {
                    return noteDate >= date && noteDate < date2;
                } else {
                    return noteDate >= date;
                }
            });
        }
    }
}