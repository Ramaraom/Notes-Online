export interface NoteModel {
    id: number;
    type: string;
    content: string;
    dateTime: Date;
    image: string;
}
