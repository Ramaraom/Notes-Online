import { NgModule } from '@angular/core';
import { NotesService } from './notes.service';
import {NotesFilter} from './notes-filter.pipe';
import { AppCoreModule } from '../core/core.module';


@NgModule({
    imports: [AppCoreModule],
    exports: [NotesFilter],
    declarations: [NotesFilter],
    providers: [NotesService],
})
export class SharedModule { }

