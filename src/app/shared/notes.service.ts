import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, } from 'rxjs/operators';
import { Observable, } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import * as model from './models';

type noteModelArray = Array<model.NoteModel>;

@Injectable()
export class NotesService {

    public notesData: noteModelArray = [];
    public $notesObservable = of(this.notesData);
    constructor(private httpClient: HttpClient) { }

    getData(): noteModelArray {
        return this.notesData;
    }
    addData(note: model.NoteModel): void {
        if (note.id == null || note.id === undefined) {
            note.id = this.notesData.length + 1;
        }
        this.notesData.push(note);
    }
    updateData(note: model.NoteModel): void {
        let resNote = this.notesData.filter((item) => {
            return item.id === note.id;
        })[0];
        resNote.content = note.content;
        resNote.dateTime = note.dateTime;
        resNote.type = note.type;
        resNote.image = note.image;
    }
    filterSearch(text: string): any {
        if(text.length == 0) {
           // return this.notesData;
        }
        else {
            this.notesData= this.notesData.filter((note) => { 
                return note.content.indexOf(text)>-1;
            });
            //this.notesData.pop();
        }
    }
    deleteNote(note: model.NoteModel): void {
        const noteIndex = this.notesData.findIndex((item) => item.id === note.id);
        this.notesData.splice(noteIndex, 1);
    }
}
